import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Mahasiswa burhan = new Mahasiswa("burhan", "123", "ilkom");
        Mahasiswa dekDepe = new Mahasiswa("dekDepe", "456", "ilkom");
        Mahasiswa kakPewe = new Mahasiswa("kakPewe", "789", "ilkom");
        Mahasiswa[] paraMahasiswa = {burhan, dekDepe, kakPewe};
        System.out.println(getNames(paraMahasiswa));

    }
    public static String getNames(Mahasiswa[] paraMahasiswa) {
        String names = "";
        for(int i=0; i<paraMahasiswa.length; i++) {
            names+= paraMahasiswa[i].getNama()+ " ";
        }
        return names;
    }

}
